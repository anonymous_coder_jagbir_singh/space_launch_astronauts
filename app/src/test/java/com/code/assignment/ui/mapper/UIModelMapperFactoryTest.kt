package com.code.assignment.ui.mapper

import com.nhaarman.mockitokotlin2.any
import com.code.assignment.UITestCase
import com.code.assignment.data.data.mapper.DataModelMapperFactory
import com.code.assignment.domain.usecase.model.Astronaut
import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.domain.usecase.model.AstronautModel
import com.code.assignment.ui.resources.StringResources
import com.code.assignment.ui.screen.astronauts.mapper.UIAstronautsMapper
import com.code.assignment.ui.screen.astronauts.model.UIAstronautsModel
import com.code.assignment.ui.screen.detailed.mapper.UIAstronautDetailsMapper
import com.code.assignment.ui.screen.detailed.model.UIAstronautDetails
import com.code.assignment.ui.uicomponent.model.UIItem
import junit.framework.TestCase
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class UIModelMapperFactoryTest : UITestCase() {

    @Mock
    lateinit var stringResources: StringResources

    override fun setUp() {
        super.setUp()
        Mockito.`when`(stringResources.getString(any())).thenReturn("")
    }

    @Test
    fun uiAstronautsMapperTest() {
        TestCase.assertEquals(
                "The mapper instance returned is not of type: "
                        + UIAstronautsMapper::class.java,
                UIModelMapperFactory(stringResources).create<UIAstronautsModel, AstronautModel>(
                        UIAstronautsModel::class.java
                ).javaClass,
                UIAstronautsMapper::class.java
        )
    }

    @Test
    fun uiAstronautDetailsMapperTest() {
        TestCase.assertEquals(
                "The mapper instance returned is not of type: "
                        + UIAstronautDetailsMapper::class.java,
                UIModelMapperFactory(stringResources).create<UIAstronautDetails, AstronautDetails>(
                        UIAstronautDetails::class.java
                ).javaClass,
                UIAstronautDetailsMapper::class.java
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun noMapperFoundTest() {
        DataModelMapperFactory().create<UIItem, Astronaut>(UIItem::class.java).javaClass
    }

}