package com.code.assignment.ui.screen.astronauts

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.times
import com.code.assignment.UITestCase
import com.code.assignment.domain.usecase.astronauts.FetchAstronautsUseCase
import com.code.assignment.ui.mapper.UIModelMapperFactory
import com.code.assignment.ui.resources.StringResources
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.screen.common.UIHeaderItem
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class AstronautsViewModelTest : UITestCase() {

    private lateinit var viewModel: AstronautsViewModel

    @Mock
    private lateinit var masterListener: AstronautsViewModel.Listener

    @Mock
    private lateinit var astronautsUseCase: FetchAstronautsUseCase

    @Mock
    lateinit var stringResources: StringResources

    override fun setUp() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(stringResources.getString(any())).thenReturn("")
        viewModel = Mockito.spy(
                AstronautsViewModel(
                        fetchAstronautsUseCase = astronautsUseCase,
                        uiModelMapperFactory = UIModelMapperFactory(stringResources)
                )
        )
        viewModel.setMasterViewModelCommListener(masterListener)
        viewModel.fetchAstronauts()
    }

    @Test
    fun onLoadAstronautsTest() {
        Mockito.verify(astronautsUseCase).execute(any(), eq(null))
        Mockito.verify(masterListener, times(1)).toggleBusyIndicator(true)
    }

    @Test
    fun onItemClickedTest() {
        val item = UIAstronautItem(
                123,
                "name",
                "nationality",
                "/image_url.jpg",
                "retired"
        )
        viewModel.onItemClicked(item)
        Mockito.verify(masterListener).onAstronautDetailsRequested(item)
    }

    @Test
    fun onUnrecognizedItemClickedTest() {
        val item = UIHeaderItem("")
        viewModel.onItemClicked(item)
        Mockito.verify(masterListener).showToast(any())
    }
}