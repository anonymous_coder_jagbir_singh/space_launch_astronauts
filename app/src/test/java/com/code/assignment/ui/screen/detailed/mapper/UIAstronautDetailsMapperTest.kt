package com.code.assignment.ui.screen.detailed.mapper

import com.nhaarman.mockitokotlin2.any
import com.code.assignment.UITestCase
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.ui.resources.StringResources
import com.code.assignment.ui.screen.detailed.model.UIAstronautDetails
import com.code.assignment.ui.uicomponent.model.UIModel
import junit.framework.TestCase
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class UIAstronautDetailsMapperTest : UITestCase() {

    private val domain: SResult<AstronautDetails>
        get() {
            val model = AstronautDetails(
                    1,
                    "asdfghjk",
                    "qwerty",
                    "zxcvbn",
                    "02-02-02",
                    "qwertyuiop asdfghjkl zxcvbnm",
                    "/qwertyuio.jpg",
                    "status"
            )
            return SResult.Success(model)
        }

    @Mock
    lateinit var stringResources: StringResources

    override fun setUp() {
        super.setUp()
        Mockito.`when`(stringResources.getString(any())).thenReturn("")
    }

    @Test
    fun uiNowPlayingMapperTest() {
        val mapper = UIAstronautDetailsMapper(stringResources)
        val uiModel = mapper.fromDomain(domain)
        val ui = uiModel as? UIModel.Success<UIAstronautDetails>
        TestCase.assertTrue(ui != null)
        TestCase.assertEquals("asdfghjk", ui?.data?.name)
    }
}