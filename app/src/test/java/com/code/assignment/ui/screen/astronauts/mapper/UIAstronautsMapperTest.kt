package com.code.assignment.ui.screen.astronauts.mapper

import com.nhaarman.mockitokotlin2.any
import com.code.assignment.UITestCase
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.model.Astronaut
import com.code.assignment.domain.usecase.model.AstronautModel
import com.code.assignment.ui.resources.StringResources
import com.code.assignment.ui.screen.astronauts.model.UIAstronautsModel
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.screen.common.UIHeaderItem
import com.code.assignment.ui.uicomponent.model.UIModel
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class UIAstronautsMapperTest : UITestCase() {

    private val domain: SResult<AstronautModel>
        get() {
            val model = AstronautModel(
                    count = 600,
                    results = listOf(
                            default
                    )
            )
            return SResult.Success(model)
        }

    private val default: Astronaut
        get() {
            return Astronaut(
                    1,
                    "asdfghjk",
                    "qwerty",
                    "zxcvbn",
                    "status"
            )
        }

    @Mock
    lateinit var stringResources: StringResources

    override fun setUp() {
        super.setUp()
        Mockito.`when`(stringResources.getString(any())).thenReturn("")
    }

    @Test
    fun uiAstronautsMapperTest() {
        val mapper = UIAstronautsMapper(stringResources)
        val uiModel = mapper.fromDomain(domain)
        val ui = uiModel as? UIModel.Success<UIAstronautsModel>
        assertTrue(ui != null)
        assertEquals(2, ui?.data?.items?.size ?: 0)
        val item = ui?.data?.items?.get(1) as? UIAstronautItem
        assertEquals(UIAstronautItem::class.java, item?.javaClass)
        assertEquals(UIHeaderItem::class.java, ui?.data?.items?.get(0)?.javaClass)
    }
}