package com.code.assignment.ui.screen.detailed

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.eq
import com.code.assignment.UITestCase
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.austronautdetail.FetchAstronautDetailsUseCase
import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.ui.mapper.UIModelMapperFactory
import com.code.assignment.ui.resources.StringResources
import io.reactivex.observers.DisposableObserver
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.spy
import org.mockito.MockitoAnnotations

class AstronautDetailsViewModelTest : UITestCase() {

    private lateinit var viewModel: AstronautDetailsViewModel

    @Mock
    private lateinit var masterListener: AstronautDetailsViewModel.Listener

    @Mock
    private lateinit var usecase: FetchAstronautDetailsUseCase

    @Mock
    lateinit var stringResources: StringResources

    override fun setUp() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(stringResources.getString(any())).thenReturn("")
        viewModel = spy(
                AstronautDetailsViewModel(
                        usecase,
                        UIModelMapperFactory(stringResources)
                )
        )
    }

    @Test
    fun onLoadAstronautDetailsTest() {
        val id = 123
        val captor = argumentCaptor<DisposableObserver<SResult<AstronautDetails>>>()
        Mockito.`when`(masterListener.getAstronautId()).thenReturn(id)
        viewModel.setMasterViewModelCommListener(masterListener)
        viewModel.fetchAstronautDetails()
        Mockito.verify(masterListener).toggleBusyIndicator(true)
        Mockito.verify(masterListener).getAstronautId()
        Mockito.verify(usecase).execute(captor.capture(), eq(id))
    }

    @Test
    fun onUpRequestedTest() {
        viewModel.setMasterViewModelCommListener(masterListener)
        viewModel.onUpButtonClicked()
        Mockito.verify(masterListener).onUpButtonClicked()
    }

}