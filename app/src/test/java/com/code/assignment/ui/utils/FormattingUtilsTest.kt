package com.code.assignment.ui.utils

import org.junit.Assert.assertEquals
import org.junit.Test

class FormattingUtilsTest {

    @Test
    fun toFormattedDateTest() {
        assertEquals("Dec 02, 1980", FormattingUtils.toFormattedDate("1980-12-2"))
    }

}