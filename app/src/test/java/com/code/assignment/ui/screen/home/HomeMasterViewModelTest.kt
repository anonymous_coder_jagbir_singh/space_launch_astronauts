package com.code.assignment.ui.screen.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.code.assignment.UITestCase
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.uicomponent.model.UIError
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class HomeMasterViewModelTest : UITestCase() {

    private lateinit var viewModel: HomeMasterViewModel

    @Mock
    private lateinit var progressBarObserver: Observer<Boolean?>

    @Mock
    private lateinit var errorToastObserver: Observer<UIError?>

    @Mock
    private lateinit var popBackStackObserver: Observer<Unit?>

    @get:Rule
    val rule = InstantTaskExecutorRule()

    override fun setUp() {
        super.setUp()

        viewModel = HomeMasterViewModel()

        viewModel.progressVisibilityLiveData.observeForever(progressBarObserver)
        viewModel.errorToastLiveData.observeForever(errorToastObserver)
        viewModel.popBackStackLiveData.observeForever(popBackStackObserver)
    }

    @Test
    fun progressBarTest() {
        viewModel.toggleBusyIndicator(true)
        Mockito.verify(progressBarObserver).onChanged(true)
    }

    @Test
    fun errorToastTest() {
        val captor = argumentCaptor<UIError>()
        val errorMsg = "error msg"
        val uiError = UIError(errorMsg)
        viewModel.showToast(uiError)
        Mockito.verify(errorToastObserver).onChanged(captor.capture())
        assertEquals(errorMsg, captor.firstValue.message)
    }

    @Test
    fun initTest() {
        viewModel.init()

        assertEquals(viewModel.fragmentMovementLiveData.value?.masterListenerClass, HomeMasterViewModel::class.java)
        assertEquals(viewModel.fragmentMovementLiveData.value?.fragmentType, FragmentType.ASTRONAUTS)
        assertEquals(viewModel.fragmentMovementLiveData.value?.addToBackStack, false)
    }

    @Test
    fun astronautDetailsRequestedTest() {
        val uiItem = UIAstronautItem(
                123,
                "name",
                "nationality",
                "/image_url.jpg",
                "retired"
        )
        viewModel.onAstronautDetailsRequested(uiItem)

        assertEquals(uiItem.id, viewModel.getAstronautId())
        assertEquals(viewModel.fragmentMovementLiveData.value?.masterListenerClass, HomeMasterViewModel::class.java)
        assertEquals(viewModel.fragmentMovementLiveData.value?.fragmentType, FragmentType.ASTRONAUT_DETAILS)
        assertEquals(viewModel.fragmentMovementLiveData.value?.addToBackStack, true)
        assertNull(viewModel.fragmentMovementLiveData.value?.params)
    }

    @Test
    fun onUpLevelRequestedTest() {
        viewModel.onUpButtonClicked()
        Mockito.verify(popBackStackObserver).onChanged(Unit)
    }
}