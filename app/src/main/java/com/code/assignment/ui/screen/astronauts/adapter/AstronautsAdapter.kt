package com.code.assignment.ui.screen.astronauts.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.code.assignment.R
import com.code.assignment.databinding.ItemAstronautBinding
import com.code.assignment.databinding.ItemHeaderBinding
import com.code.assignment.ui.screen.astronauts.viewholder.UIAstronautViewHolder
import com.code.assignment.ui.screen.astronauts.viewholder.UIHeaderViewHolder
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.uicomponent.adapter.SAdapter
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponent.model.UIItem
import com.code.assignment.ui.uicomponent.viewholder.SItemViewHolder

class AstronautsAdapter(
        listener: OnItemViewClickListener<UIItem>
) : SAdapter<OnItemViewClickListener<UIItem>>(listener) {

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): SItemViewHolder<*, UIItem, OnItemViewClickListener<UIItem>> {
        val viewHolder = when (viewType) {

            UIAstronautItem.ITEM_VIEW_TYPE -> {
                val binding =
                        DataBindingUtil.inflate<ItemAstronautBinding>(
                                LayoutInflater.from(parent.context),
                                R.layout.item_astronaut,
                                parent,
                                false
                        )
                UIAstronautViewHolder(
                        binding,
                        listener
                )
            }

            else -> {
                //UIHeaderItem is considered default here
                val binding =
                        DataBindingUtil.inflate<ItemHeaderBinding>(
                                LayoutInflater.from(parent.context),
                                R.layout.item_header,
                                parent,
                                false
                        )
                UIHeaderViewHolder(
                        binding,
                        listener
                )
            }
        }
        return viewHolder as SItemViewHolder<*, UIItem, OnItemViewClickListener<UIItem>>
    }
}