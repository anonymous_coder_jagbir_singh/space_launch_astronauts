package com.code.assignment.ui.screen.detailed


import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.code.assignment.R
import com.code.assignment.databinding.FragmentAstronautDetailsBinding
import com.code.assignment.ui.child.BaseWorkflowFragment
import com.code.assignment.ui.utils.ImageUtils
import com.code.assignment.ui.utils.setMargin

class AstronautDetailsFragment : BaseWorkflowFragment<AstronautDetailsViewModel.Listener, AstronautDetailsViewModel, FragmentAstronautDetailsBinding>() {

    companion object {

        fun <MASTER_LISTENER> newInstance(masterListenerClass: Class<MASTER_LISTENER>): AstronautDetailsFragment where MASTER_LISTENER : ViewModel, MASTER_LISTENER : AstronautDetailsViewModel.Listener {

            val fragment = AstronautDetailsFragment()

            val args = Bundle()
            args.putSerializable(ARG_MASTER_COMM_VIEW_MODEL, masterListenerClass)
            fragment.arguments = args

            return fragment
        }
    }

    override fun getViewModelClassType(): Class<AstronautDetailsViewModel> {
        return AstronautDetailsViewModel::class.java
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initLiveData()

        setupUI()

        if (savedInstanceState == null && isNewCreation) {
            viewModel.fetchAstronautDetails()
        }
    }

    private fun initLiveData() {
        viewModel.astronautDetailsLiveData.observe(this, Observer {
            it?.apply {
                binding.dateOfBirth.text = dateOfBirth
                ImageUtils.loadImage(
                        imageView = binding.imageView.logo,
                        url = image
                )
                binding.name.text = name
                binding.bioDesc.text = bio
                binding.nationality.text = nationality
                binding.statusLayout.desc.text = status
            }
        })
    }

    private fun setupUI() {
        binding.imageView.logo.apply {
            val margin = resources.getDimensionPixelSize(R.dimen.dp_2)
            setMargin(margin)
        }
        binding.toolbar.setNavigationOnClickListener {
            viewModel.onUpButtonClicked()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_astronaut_details
    }
}
