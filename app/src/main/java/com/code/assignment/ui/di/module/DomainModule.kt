package com.code.assignment.ui.di.module

import com.code.assignment.data.data.APIRemoteRepository
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.thread.ObserverThread
import com.code.assignment.ui.thread.UIThread
import dagger.Binds
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
abstract class DomainModule {

    @Binds
    internal abstract fun bindObserverThread(uiThread: UIThread): ObserverThread

    @Binds
    @Singleton
    internal abstract fun bindRemoteRepository(remoteRepository: APIRemoteRepository): RemoteRepository

    @Module
    companion object {
        @JvmStatic
        @Provides
        @Singleton
        internal fun provideExecutor(): Executor {
            return Executors.newFixedThreadPool(2)
        }
    }

}