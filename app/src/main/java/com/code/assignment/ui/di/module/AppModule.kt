package com.code.assignment.ui.di.module

import android.content.Context
import com.code.assignment.ui.base.SApplication
import com.code.assignment.ui.display.AppDisplayInfo
import com.code.assignment.ui.display.DisplayInfo
import com.code.assignment.ui.resources.SStringResources
import com.code.assignment.ui.resources.StringResources
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Dagger Module to satisfy all dependencies in app layer
 */
@Module
class AppModule {
    @Provides
    @Singleton
    internal fun provideDisplayInfo(context: Context): DisplayInfo {
        return AppDisplayInfo(context.resources.displayMetrics)
    }

    @Provides
    @Singleton
    internal fun provideStringResources(context: Context): StringResources {
        return SStringResources(context.resources)
    }

    @Singleton
    @Provides
    internal fun provideContext(application: SApplication): Context {
        return application
    }
}