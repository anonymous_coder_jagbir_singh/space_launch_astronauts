package com.code.assignment.ui.screen.detailed.mapper

import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.ui.mapper.UIBaseMapper
import com.code.assignment.ui.resources.StringResources
import com.code.assignment.ui.screen.detailed.model.UIAstronautDetails
import com.code.assignment.ui.utils.FormattingUtils

class UIAstronautDetailsMapper(stringResources: StringResources) :
        UIBaseMapper<UIAstronautDetails, AstronautDetails>(stringResources) {

    override fun getModel(domainModel: AstronautDetails): UIAstronautDetails {
        domainModel.apply {
            return UIAstronautDetails(
                    id = id,
                    name = name,
                    nationality = nationality,
                    image = image,
                    dateOfBirth = FormattingUtils.toFormattedDate(dateOfBirth) ?: "--",
                    bio = bio,
                    status = status
            )
        }
    }
}