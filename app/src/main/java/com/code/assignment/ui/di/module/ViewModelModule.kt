package com.code.assignment.ui.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.code.assignment.ui.di.SViewModelFactory
import com.code.assignment.ui.di.annotation.ViewModelKey
import com.code.assignment.ui.screen.astronauts.AstronautsViewModel
import com.code.assignment.ui.screen.detailed.AstronautDetailsViewModel
import com.code.assignment.ui.screen.home.HomeMasterViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(AstronautsViewModel::class)
    internal abstract fun bindAstronautsViewModel(viewModel: AstronautsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeMasterViewModel::class)
    internal abstract fun bindHomeMasterViewModel(viewModel: HomeMasterViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AstronautDetailsViewModel::class)
    internal abstract fun bindAstronautDetailsViewModel(viewModel: AstronautDetailsViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: SViewModelFactory): ViewModelProvider.Factory


}