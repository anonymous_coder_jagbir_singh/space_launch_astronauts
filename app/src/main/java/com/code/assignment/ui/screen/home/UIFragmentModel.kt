package com.code.assignment.ui.screen.home

import androidx.lifecycle.ViewModel

/**
 * The model that provides all the details that are needed to
 * instantiate a fragment based on [FragmentType]
 */
class UIFragmentModel<MASTER_LISTENER : ViewModel>(
        val fragmentType: FragmentType = FragmentType.ASTRONAUTS,
        val addToBackStack: Boolean = false,
        val params: Any? = null,
        val masterListenerClass: Class<MASTER_LISTENER>,
        val animation: UIAnimation = UIAnimation()
)