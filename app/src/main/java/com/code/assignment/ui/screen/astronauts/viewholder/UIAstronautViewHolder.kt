package com.code.assignment.ui.screen.astronauts.viewholder

import com.code.assignment.R
import com.code.assignment.databinding.ItemAstronautBinding
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponent.model.UIItem
import com.code.assignment.ui.uicomponent.viewholder.SItemViewHolder
import com.code.assignment.ui.utils.ImageUtils
import com.code.assignment.ui.utils.setMargin

class UIAstronautViewHolder(
        binding: ItemAstronautBinding,
        listener: OnItemViewClickListener<UIItem>
) : SItemViewHolder<ItemAstronautBinding, UIAstronautItem, OnItemViewClickListener<UIItem>>(
        binding,
        listener
) {

    init {
        binding.imageView.logo.apply {
            val margin = resources.getDimensionPixelSize(R.dimen.dp_1)
            setMargin(margin)
        }
    }

    override fun bind(item: UIAstronautItem, index: Int) {
        setOnClickListener(item)
        item.apply {
            ImageUtils.loadImage(
                    imageView = binding.imageView.logo,
                    url = thumbnail
            )
            binding.name.text = name
            binding.status.text = status
            binding.nationality.text = nationality
        }
    }
}