package com.code.assignment.ui.display

interface DisplayInfo {

    val density: Float

    val screenWidthPx: Int

    val screenHeightPx: Int

    fun dpToPx(dp: Int): Int

}