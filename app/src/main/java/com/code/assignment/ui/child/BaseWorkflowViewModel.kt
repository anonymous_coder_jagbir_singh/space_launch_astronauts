package com.code.assignment.ui.child

import androidx.lifecycle.ViewModel

/**
 *
 * This is the base class for any view model (along with its fragment) that can be re-used in any workflow.
 * This view model provides updates to the master/controller via [MASTER_COMM_INTERFACE].
 *
 * Please refer to [BaseWorkflowFragment] for more documentation about the flow/design.
 *
 */
abstract class BaseWorkflowViewModel<MASTER_VIEW_MODEL_COMM_LISTENER> : ViewModel() {

    protected var masterListener: MASTER_VIEW_MODEL_COMM_LISTENER? = null

    fun setMasterViewModelCommListener(listener: MASTER_VIEW_MODEL_COMM_LISTENER) {
        masterListener = listener
        onMasterCommListenerSet()
    }

    protected abstract fun onMasterCommListenerSet()
}