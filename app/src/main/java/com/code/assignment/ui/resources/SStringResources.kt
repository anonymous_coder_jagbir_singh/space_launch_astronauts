package com.code.assignment.ui.resources

import android.content.res.Resources
import javax.inject.Inject

internal class SStringResources @Inject constructor(private val resources: Resources) : StringResources {

    override fun getString(resId: Int, vararg formatArgs: String): String {
        return resources.getString(resId, *formatArgs)
    }

}