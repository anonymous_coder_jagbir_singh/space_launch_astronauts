package com.code.assignment.ui.di.module

import com.code.assignment.ui.screen.astronauts.AstronautsFragment
import com.code.assignment.ui.screen.detailed.AstronautDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    internal abstract fun bindAstronautsFragment(): AstronautsFragment

    @ContributesAndroidInjector
    internal abstract fun bindAstronautDetailsFragment(): AstronautDetailsFragment

}