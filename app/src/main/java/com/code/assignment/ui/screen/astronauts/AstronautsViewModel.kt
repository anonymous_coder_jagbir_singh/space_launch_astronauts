package com.code.assignment.ui.screen.astronauts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.astronauts.FetchAstronautsUseCase
import com.code.assignment.domain.usecase.model.AstronautModel
import com.code.assignment.ui.child.BaseWorkflowViewModel
import com.code.assignment.ui.listener.BusyIndicatorRequestListener
import com.code.assignment.ui.listener.MessageRequestListener
import com.code.assignment.ui.mapper.UIModelMapperFactory
import com.code.assignment.ui.screen.astronauts.model.UIAstronautsModel
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.uicomponent.model.UIError
import com.code.assignment.ui.uicomponent.model.UIItem
import com.code.assignment.ui.uicomponent.model.UIModel
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class AstronautsViewModel @Inject constructor(
        private val fetchAstronautsUseCase: FetchAstronautsUseCase,
        private val uiModelMapperFactory: UIModelMapperFactory
) : BaseWorkflowViewModel<AstronautsViewModel.Listener>() {

    private val _itemsLiveData: MutableLiveData<List<UIItem>?> = MutableLiveData()
    val itemsLiveData = _itemsLiveData as LiveData<List<UIItem>?>

    private val items: MutableList<UIItem> = mutableListOf()

    override fun onCleared() {
        fetchAstronautsUseCase.dispose()
        super.onCleared()
    }

    override fun onMasterCommListenerSet() {
    }

    fun fetchAstronauts() {
        masterListener?.toggleBusyIndicator(true)
        fetchAstronautsUseCase.execute(object : DisposableObserver<SResult<AstronautModel>>() {
            override fun onComplete() {
                //Do nothing
            }

            override fun onNext(t: SResult<AstronautModel>) {
                val model =
                        uiModelMapperFactory.create<UIAstronautsModel, SResult<AstronautModel>>(
                                UIAstronautsModel::class.java
                        ).fromDomain(t)
                when (model) {
                    is UIModel.Success -> {
                        items.clear()
                        items.addAll(model.data.items)
                        _itemsLiveData.postValue(items)
                    }
                    is UIModel.Error -> masterListener?.showToast(model.error)
                }
                masterListener?.toggleBusyIndicator(false)
            }

            override fun onError(e: Throwable) {
                masterListener?.showToast(UIError())
                masterListener?.toggleBusyIndicator(false)
            }

        }, null)
    }

    fun onItemClicked(item: UIItem) {
        when (item) {
            is UIAstronautItem -> masterListener?.onAstronautDetailsRequested(item)
            /*
                Other items shall be handled in future, as required.
             */
            else -> masterListener?.showToast(UIError())
        }
    }

    fun onSortByNameClicked() {
        items.sortWith(
                compareBy({
                    when (it) {
                        is UIAstronautItem -> 1
                        else -> -1
                    }
                }, {
                    when (it) {
                        is UIAstronautItem -> {
                            it.name
                        }
                        else -> 0
                    }
                })
        )
        _itemsLiveData.postValue(items)
    }

    fun onSortByStatusClicked() {
        items.sortWith(
                compareBy({
                    when (it) {
                        is UIAstronautItem -> 1
                        else -> -1
                    }
                }, {
                    when (it) {
                        is UIAstronautItem -> {
                            it.status
                        }
                        else -> 0
                    }
                })
        )
        _itemsLiveData.postValue(items)
    }

    fun onRefreshClicked() {
        fetchAstronauts()
    }

    /**
     * The interface which enables communication with the
     * master/controller
     */
    interface Listener : BusyIndicatorRequestListener, MessageRequestListener {

        fun onAstronautDetailsRequested(item: UIAstronautItem)

    }
}