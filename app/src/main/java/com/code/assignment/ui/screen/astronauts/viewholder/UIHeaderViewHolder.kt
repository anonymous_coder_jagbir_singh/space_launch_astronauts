package com.code.assignment.ui.screen.astronauts.viewholder

import com.code.assignment.databinding.ItemHeaderBinding
import com.code.assignment.ui.screen.common.UIHeaderItem
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponent.model.UIItem
import com.code.assignment.ui.uicomponent.viewholder.SItemViewHolder

class UIHeaderViewHolder(
        binding: ItemHeaderBinding,
        listener: OnItemViewClickListener<UIItem>
) : SItemViewHolder<ItemHeaderBinding, UIHeaderItem, OnItemViewClickListener<UIItem>>(
        binding,
        listener
) {

    override fun bind(item: UIHeaderItem, index: Int) {
        binding.heading.text = item.text
    }
}