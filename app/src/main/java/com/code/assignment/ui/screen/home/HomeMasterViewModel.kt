package com.code.assignment.ui.screen.home

import androidx.lifecycle.ViewModel
import com.code.assignment.R
import com.code.assignment.ui.base.SingleLiveEvent
import com.code.assignment.ui.screen.astronauts.AstronautsViewModel
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.screen.detailed.AstronautDetailsViewModel
import com.code.assignment.ui.uicomponent.model.UIError
import javax.inject.Inject

/**
 * [ViewModel] which manages all the view related data that is to be fed into [HomeActivity]
 *
 * There is no reference to any View and/or Activity instance in this class.
 * Data is provided to the "UI"/view via LiveData objects.
 *
 * Each LiveData object controls the smallest meaningful view component within the entire
 * Activity layout, as required.
 *
 */
class HomeMasterViewModel @Inject constructor() : ViewModel(),
        AstronautsViewModel.Listener,
        AstronautDetailsViewModel.Listener {

    val fragmentMovementLiveData =
            SingleLiveEvent<UIFragmentModel<HomeMasterViewModel>?>()

    val errorToastLiveData =
            SingleLiveEvent<UIError?>()
    val progressVisibilityLiveData =
            SingleLiveEvent<Boolean?>()
    val popBackStackLiveData =
            SingleLiveEvent<Unit?>()

    /*
        The data is being shared between the Fragments via the
        Master View Model.
        Since data persistence is not mandatory for the assignment,
        this approach still helps to make sure the data is persisted
        during configuration changes.
     */
    private var selected: UIAstronautItem? = null


    fun init() {
        fragmentMovementLiveData.postValue(
                UIFragmentModel(
                        fragmentType = FragmentType.ASTRONAUTS,
                        masterListenerClass = HomeMasterViewModel::class.java
                )
        )
    }

    override fun onAstronautDetailsRequested(item: UIAstronautItem) {
        selected = item
        fragmentMovementLiveData.postValue(
                UIFragmentModel(
                        fragmentType = FragmentType.ASTRONAUT_DETAILS,
                        masterListenerClass = HomeMasterViewModel::class.java,
                        addToBackStack = true,
                        animation = UIAnimation(
                                enter = R.anim.slide_in_right,
                                exit = 0,
                                popEnter = 0,
                                popExit = R.anim.slide_out_right
                        )
                )
        )
    }

    override fun getAstronautId(): Int? {
        return selected?.id
    }

    override fun toggleBusyIndicator(show: Boolean) {
        progressVisibilityLiveData.postValue(show)
    }

    override fun showToast(uiError: UIError?) {
        errorToastLiveData.postValue(uiError)
    }

    override fun onUpButtonClicked() {
        popBackStackLiveData.postValue(Unit)
    }

    fun onBackPressed() {
        if (progressVisibilityLiveData.value == true) {
            progressVisibilityLiveData.postValue(false)
        }
    }
}