package com.code.assignment.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.code.assignment.R
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

/**
 * A generic base Activity class which defines and handles the following for each child Activity instance:
 *
 * - The view bindings of type [T]
 * - The view model [VIEW_MODEL] which is to be associated with the respective Activity.
 * - Handles Dagger injections into the actual instance of each [Activity], which inherits from this class.
 *
 */
abstract class BaseActivity<T : ViewDataBinding, VIEW_MODEL : ViewModel> :
        DaggerAppCompatActivity() {

    protected lateinit var binding: T

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var viewModel: VIEW_MODEL

    /**
     * The layout resource of the activity
     */
    @LayoutRes
    protected abstract fun getLayoutResource(): Int

    /**
     * The class type of the [ViewModel] that is to be associated with the inheriting activity
     * This is needed to pass the class type in order to get the
     * respective [ViewModel] from [ViewModelProviders]
     */
    protected abstract val viewModelClassType: Class<VIEW_MODEL>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, getLayoutResource())
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClassType)
    }

    override fun onDestroy() {
        binding.unbind()
        super.onDestroy()
    }

    /**
     * Show a toast.
     * In case a null string is passed, the default error message is shown:
     * [R.string.unknown_error]
     */
    protected fun showToast(message: String?) {
        Toast.makeText(this, message ?: getString(R.string.unknown_error), Toast.LENGTH_LONG).show()
    }
}