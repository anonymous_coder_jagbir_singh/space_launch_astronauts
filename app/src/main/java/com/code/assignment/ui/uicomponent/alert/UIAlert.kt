package com.code.assignment.ui.uicomponent.alert

abstract class UIAlert(
        val title: String?,
        val description: String?,
        val cancelable: Boolean
)