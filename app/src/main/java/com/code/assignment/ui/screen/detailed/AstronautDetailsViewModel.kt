package com.code.assignment.ui.screen.detailed

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.austronautdetail.FetchAstronautDetailsUseCase
import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.ui.child.BaseWorkflowViewModel
import com.code.assignment.ui.listener.BusyIndicatorRequestListener
import com.code.assignment.ui.listener.MessageRequestListener
import com.code.assignment.ui.listener.UpLevelRequestListener
import com.code.assignment.ui.mapper.UIModelMapperFactory
import com.code.assignment.ui.screen.detailed.model.UIAstronautDetails
import com.code.assignment.ui.uicomponent.model.UIError
import com.code.assignment.ui.uicomponent.model.UIModel
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class AstronautDetailsViewModel @Inject constructor(
        private val fetchAstronautDetailsUseCase: FetchAstronautDetailsUseCase,
        private val uiModelMapperFactory: UIModelMapperFactory
) : BaseWorkflowViewModel<AstronautDetailsViewModel.Listener>() {

    private val _astronautDetailsLiveData: MutableLiveData<UIAstronautDetails?> = MutableLiveData()
    val astronautDetailsLiveData = _astronautDetailsLiveData as LiveData<UIAstronautDetails?>

    override fun onCleared() {
        fetchAstronautDetailsUseCase.dispose()
        super.onCleared()
    }

    override fun onMasterCommListenerSet() {

    }

    fun fetchAstronautDetails() {
        masterListener?.toggleBusyIndicator(true)
        fetchAstronautDetailsUseCase.execute(object : DisposableObserver<SResult<AstronautDetails>>() {
            override fun onComplete() {
                //Do nothing
            }

            override fun onNext(t: SResult<AstronautDetails>) {
                val model =
                        uiModelMapperFactory.create<UIAstronautDetails, SResult<AstronautDetails>>(
                                UIAstronautDetails::class.java
                        ).fromDomain(t)
                when (model) {
                    is UIModel.Success -> {
                        _astronautDetailsLiveData.postValue(model.data)
                    }
                    is UIModel.Error -> masterListener?.showToast(model.error)
                }
                masterListener?.toggleBusyIndicator(false)
            }

            override fun onError(e: Throwable) {
                masterListener?.showToast(UIError())
                masterListener?.toggleBusyIndicator(false)
            }

        }, masterListener?.getAstronautId())
    }

    fun onUpButtonClicked() {
        masterListener?.onUpButtonClicked()
    }

    /**
     * The interface which enables communication with the
     * master/controller
     */
    interface Listener : BusyIndicatorRequestListener,
            MessageRequestListener,
            UpLevelRequestListener {

        fun getAstronautId(): Int?
    }
}