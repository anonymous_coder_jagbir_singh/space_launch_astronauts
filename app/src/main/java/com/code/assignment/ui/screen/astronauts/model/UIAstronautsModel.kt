package com.code.assignment.ui.screen.astronauts.model

import com.code.assignment.ui.uicomponent.model.UIItem

class UIAstronautsModel(
        val items: List<UIItem>
)