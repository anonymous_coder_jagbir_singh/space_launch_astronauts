package com.code.assignment.ui.screen.common

import com.code.assignment.ui.uicomponent.model.UIItem
import java.util.*

class UIHeaderItem(
        val text: String
) : UIItem {
    override val id: Int
        get() = 0x0200

    companion object {
        val ITEM_VIEW_TYPE = Objects.hash("UIHeaderItem")
    }

    override val itemViewType: Int
        get() = ITEM_VIEW_TYPE
}