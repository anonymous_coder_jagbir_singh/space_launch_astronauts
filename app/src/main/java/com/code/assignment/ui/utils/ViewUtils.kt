package com.code.assignment.ui.utils

import android.content.res.Resources
import android.view.View
import android.view.ViewGroup

fun View.setMargin(margin: Int) {
    val params = this.layoutParams as ViewGroup.MarginLayoutParams
    params.setMargins(margin, margin, margin, margin)
    this.layoutParams = params
}

fun View.setMarginTop(marginTop: Int) {
    val params = this.layoutParams as ViewGroup.MarginLayoutParams
    params.setMargins(params.leftMargin, marginTop, params.rightMargin, params.bottomMargin)
    this.layoutParams = params
}

fun Resources.statusBarHeight(): Int {
    val idStatusBarHeight = this.getIdentifier("status_bar_height", "dimen", "android")
    return if (idStatusBarHeight > 0) {
        this.getDimensionPixelSize(idStatusBarHeight)
    } else {
        0
    }
}