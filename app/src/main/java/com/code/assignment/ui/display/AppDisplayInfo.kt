package com.code.assignment.ui.display

import android.util.DisplayMetrics
import javax.inject.Inject


class AppDisplayInfo @Inject constructor(private val displayMetrics: DisplayMetrics) : DisplayInfo {

    override val density: Float = displayMetrics.density

    override val screenWidthPx: Int = displayMetrics.widthPixels

    override val screenHeightPx: Int = displayMetrics.heightPixels

    override fun dpToPx(dp: Int): Int {
        return (dp * displayMetrics.density).toInt()
    }

}