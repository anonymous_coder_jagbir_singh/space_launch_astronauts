package com.code.assignment.ui.screen.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.code.assignment.R
import com.code.assignment.databinding.ActivityMainBinding
import com.code.assignment.ui.base.BaseActivity
import com.code.assignment.ui.screen.astronauts.AstronautsFragment
import com.code.assignment.ui.screen.detailed.AstronautDetailsFragment

class HomeActivity : BaseActivity<ActivityMainBinding, HomeMasterViewModel>() {

    override val viewModelClassType: Class<HomeMasterViewModel>
        get() = HomeMasterViewModel::class.java

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initLiveData()

        if (savedInstanceState == null) {
            viewModel.init()
        }
    }

    private fun initLiveData() {
        viewModel.fragmentMovementLiveData.observe(this, Observer {
            processFragmentUpdateRequest(it)
        })

        viewModel.errorToastLiveData.observe(this, Observer {
            showToast(it?.message)
        })

        viewModel.progressVisibilityLiveData.observe(this, Observer {
            if (it != null) {
                binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        viewModel.popBackStackLiveData.observe(this, Observer {
            supportFragmentManager.popBackStack()
        })
    }

    private fun processFragmentUpdateRequest(it: UIFragmentModel<HomeMasterViewModel>?) {
        if (it != null) {
            val fragment = when (it.fragmentType) {
                FragmentType.ASTRONAUTS -> AstronautsFragment.newInstance(it.masterListenerClass)
                FragmentType.ASTRONAUT_DETAILS -> AstronautDetailsFragment.newInstance(it.masterListenerClass)
            }
            loadFragment(binding.fragmentContainer.id, fragment, it.addToBackStack, it.animation)
        }
    }

    private fun loadFragment(
            containerId: Int,
            fragment: Fragment,
            addToBackStack: Boolean,
            animation: UIAnimation
    ) {
        val fragmentTransaction = supportFragmentManager.beginTransaction().apply {
            setCustomAnimations(
                    animation.enter,
                    animation.exit,
                    animation.popEnter,
                    animation.popExit
            )
            add(containerId, fragment, fragment.javaClass.simpleName)
            if (addToBackStack) {
                addToBackStack(fragment.javaClass.simpleName)
            }
        }

        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        viewModel.onBackPressed()
        super.onBackPressed()
    }

    override fun getLayoutResource(): Int {
        return R.layout.activity_main
    }
}