package com.code.assignment.ui.screen.detailed.model

class UIAstronautDetails(
        val id: Int,
        val name: String,
        val nationality: String,
        val image: String,
        val status: String,
        val bio: String,
        val dateOfBirth: String
)