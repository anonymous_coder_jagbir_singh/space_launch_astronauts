package com.code.assignment.ui.screen.astronauts.mapper

import com.code.assignment.R
import com.code.assignment.domain.usecase.model.AstronautModel
import com.code.assignment.ui.mapper.UIBaseMapper
import com.code.assignment.ui.resources.StringResources
import com.code.assignment.ui.screen.astronauts.model.UIAstronautsModel
import com.code.assignment.ui.screen.common.UIAstronautItem
import com.code.assignment.ui.screen.common.UIHeaderItem
import com.code.assignment.ui.uicomponent.model.UIItem

class UIAstronautsMapper(stringResources: StringResources) :
        UIBaseMapper<UIAstronautsModel, AstronautModel>(stringResources) {

    override fun getModel(domainModel: AstronautModel): UIAstronautsModel {
        val items: MutableList<UIItem> = domainModel.results.map {
            UIAstronautItem(
                    id = it.id,
                    thumbnail = it.thumbnail,
                    nationality = it.nationality,
                    name = it.name,
                    status = it.status
            )
        }.toMutableList()
        items.add(0, UIHeaderItem(stringResources.getString(R.string.astronauts)))
        return UIAstronautsModel(items)
    }
}