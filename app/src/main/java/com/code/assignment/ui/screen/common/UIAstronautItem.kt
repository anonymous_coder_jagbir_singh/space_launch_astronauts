package com.code.assignment.ui.screen.common

import com.code.assignment.ui.uicomponent.model.UIItem
import java.util.*

class UIAstronautItem(
        override val id: Int,
        val name: String,
        val nationality: String,
        val thumbnail: String,
        val status: String
) : UIItem {
    companion object {
        val ITEM_VIEW_TYPE = Objects.hash("UIAstronautItem")
    }

    override val itemViewType: Int
        get() = ITEM_VIEW_TYPE
}