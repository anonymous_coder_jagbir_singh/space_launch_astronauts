package com.code.assignment.ui.screen.astronauts


import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.code.assignment.R
import com.code.assignment.databinding.FragmentAstronautsBinding
import com.code.assignment.ui.child.BaseWorkflowFragment
import com.code.assignment.ui.display.DisplayInfo
import com.code.assignment.ui.screen.astronauts.adapter.AstronautsAdapter
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponent.model.UIItem
import javax.inject.Inject

class AstronautsFragment : BaseWorkflowFragment<AstronautsViewModel.Listener, AstronautsViewModel, FragmentAstronautsBinding>() {

    companion object {

        fun <MASTER_LISTENER> newInstance(masterListenerClass: Class<MASTER_LISTENER>): AstronautsFragment where MASTER_LISTENER : ViewModel, MASTER_LISTENER : AstronautsViewModel.Listener {

            val fragment = AstronautsFragment()

            val args = Bundle()
            args.putSerializable(ARG_MASTER_COMM_VIEW_MODEL, masterListenerClass)
            fragment.arguments = args

            return fragment
        }
    }

    private lateinit var adapter: AstronautsAdapter

    @Inject
    lateinit var displayInfo: DisplayInfo

    override fun getViewModelClassType(): Class<AstronautsViewModel> {
        return AstronautsViewModel::class.java
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initLiveData()

        setupUI()

        if (savedInstanceState == null && isNewCreation) {
            viewModel.fetchAstronauts()
        }
    }

    private fun initLiveData() {
        viewModel.itemsLiveData.observe(this, Observer {
            it?.apply {
                adapter.setData(this)
            }
        })
    }

    private fun setupUI() {
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        binding.recyclerView.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))

        adapter = AstronautsAdapter(
                object : OnItemViewClickListener<UIItem> {
                    override fun onItemClicked(item: UIItem) {
                        viewModel.onItemClicked(item)
                    }
                })
        binding.recyclerView.adapter = adapter

        binding.refreshButton.setOnClickListener {
            viewModel.onRefreshClicked()
        }
        binding.nameSortButton.setOnClickListener {
            viewModel.onSortByNameClicked()
        }
        binding.statusSortButton.setOnClickListener {
            viewModel.onSortByStatusClicked()
        }
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_astronauts
    }
}
