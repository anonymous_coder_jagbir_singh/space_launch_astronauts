package com.code.assignment.ui.uicomponent.listener

import com.code.assignment.ui.uicomponent.model.UIItem

interface OnItemViewClickListener<UI_ITEM : UIItem> {

    fun onItemClicked(item: UI_ITEM)

}