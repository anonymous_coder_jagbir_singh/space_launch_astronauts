package com.code.assignment.ui.uicomponent.viewholder

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.code.assignment.ui.uicomponent.listener.OnItemViewClickListener
import com.code.assignment.ui.uicomponent.model.UIItem

/**
 * A generic view holder which contains the view data binding
 * of the respective Activity/Fragment, as needed
 *
 */
abstract class SItemViewHolder<BINDING : ViewDataBinding,
        UI_ITEM : UIItem,
        ON_CLICK_LISTENER : OnItemViewClickListener<UIItem>>(
        protected val binding: BINDING,
        protected val listener: ON_CLICK_LISTENER
) : RecyclerView.ViewHolder(binding.root) {

    abstract fun bind(item: UI_ITEM, index: Int)

    protected fun setOnClickListener(item: UI_ITEM) {
        binding.root.setOnClickListener {
            listener.onItemClicked(item)
        }
    }
}
