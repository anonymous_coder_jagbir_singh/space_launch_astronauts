package com.code.assignment.ui.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object FormattingUtils {

    fun toFormattedDate(dateString: String, format: String = "MMM dd, yyyy"): String? {
        return toDate(dateString)?.let {
            SimpleDateFormat(format, Locale.getDefault()).format(it)
        }
    }

    private fun toDate(dateString: String, format: String = "yyyy-MM-dd"): Date? {
        val formatter = SimpleDateFormat(format, Locale.getDefault())
        //No information about about timezones is provided, thus assuming everything to be UTC
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        return try {
            formatter.parse(dateString)
        } catch (e: ParseException) {
            null
        }
    }
}