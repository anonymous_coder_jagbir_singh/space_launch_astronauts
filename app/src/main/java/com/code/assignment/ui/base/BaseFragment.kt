package com.code.assignment.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.code.assignment.ui.utils.setMarginTop
import com.code.assignment.ui.utils.statusBarHeight
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 * A generic base Fragment class which defines and handles the following for each child Fragment instance:
 *
 * - The view bindings of type [T]
 * - The view model [VIEW_MODEL] which is to be associated with the respective Fragment.
 * - Handles Dagger injections into the actual instance of each Fragment, which inherits from this class.
 *
 * NOTE:
 *
 * onCreateView() has been made final since we cannot enforce super.onCreateView() to be called.
 * And in case super is not called, the bindings would not be ready and thus lead to crashes
 * when accessing binding instance.
 *
 * Thus, inherited classes wanting to do any setup can choose to do it in onViewCreated()
 *
 */
abstract class BaseFragment<T : ViewDataBinding, VIEW_MODEL : ViewModel> : DaggerFragment() {

    protected lateinit var binding: T

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var viewModel: VIEW_MODEL

    /*
        This is conjunction with savedInstanceState Bundle,
        is used to determine whether the Fragment was:
        - created new
        OR
        - was it being recreated due to a configuration
        change or restored from back-stack
     */
    protected var isNewCreation = true

    /**
     * The layout resource of the fragment
     */
    @LayoutRes
    protected abstract fun getLayoutResource(): Int

    final override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutResource(), container, false)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(getViewModelClassType())
        binding.root.setMarginTop(resources.statusBarHeight())
        return binding.root
    }

    override fun onDestroyView() {
        binding.unbind()
        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
        isNewCreation = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isNewCreation = true
    }

    protected abstract fun getViewModelClassType(): Class<VIEW_MODEL>
}