package com.code.assignment.ui.uicomponent.model

interface UIItem {

    val id: Int

    val itemViewType: Int

}