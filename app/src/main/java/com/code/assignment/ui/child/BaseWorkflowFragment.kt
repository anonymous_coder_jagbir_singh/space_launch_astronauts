package com.code.assignment.ui.child

import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.code.assignment.ui.base.BaseFragment

/**
 *
 * This is the base class for any fragment (along with its view model) that can be re-used in any workflow.
 * This fragment is controlled by its view model[WORKFLOW_VIEW_MODEL] and provides updates to the master/controller via
 * [MASTER_COMM_INTERFACE].
 *
 * This pair of fragment-view model is fully independent and can be re-used in any workflow as needed.
 *
 * This design/approach aims to create an efficient way of communication between fragments
 * and between fragment and its parent activity.
 *
 * Anyone, wanting to use this fragment-view model pair has to implement
 * [WORKFLOW_VIEW_MODEL] and listen (and react to) updates sent by [WORKFLOW_VIEW_MODEL]
 *
 * This approach keeps the communication limited to the master and workflow (slave) view models only.
 * Thus, doing away with the need for any such logic in the Activity/Fragments.
 *
 * The view models (master-slave) communicate and the views which they control (Activity/Fragment)
 * remain dumb to any logic.
 */
abstract class BaseWorkflowFragment<MASTER_COMM_INTERFACE, WORKFLOW_VIEW_MODEL : BaseWorkflowViewModel<MASTER_COMM_INTERFACE>, T : ViewDataBinding> :
        BaseFragment<T, WORKFLOW_VIEW_MODEL>() {

    companion object {
        const val ARG_MASTER_COMM_VIEW_MODEL = "ARG_MASTER_COMM_VIEW_MODEL"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModels()
    }

    private fun initViewModels() {
        val masterViewModelClass = arguments?.getSerializable(ARG_MASTER_COMM_VIEW_MODEL) as? Class<ViewModel>

        if (masterViewModelClass != null) {
            val listener = ViewModelProviders.of(requireActivity(), viewModelFactory).get<ViewModel>(masterViewModelClass)
            viewModel.setMasterViewModelCommListener(listener as MASTER_COMM_INTERFACE)
        } else {
            throw IllegalArgumentException("No Master View Model Class instance was provided via argument - \"$ARG_MASTER_COMM_VIEW_MODEL\" for fragment: ${this.javaClass.simpleName}")
        }
    }

}