package com.code.assignment.ui.utils

import android.text.SpannableString
import android.text.Spanned
import android.text.style.AbsoluteSizeSpan
import android.text.style.SuperscriptSpan

fun String.toSpannableWithSuperscript(superscriptTextSize: Int, superscriptSubStr: String): SpannableString {
    val spannableString = SpannableString(this)
    spannableString.setSpan(
            SuperscriptSpan(),
            this.indexOf(superscriptSubStr),
            this.indexOf(superscriptSubStr) + superscriptSubStr.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    spannableString.setSpan(
            AbsoluteSizeSpan(superscriptTextSize),
            this.indexOf(superscriptSubStr),
            this.indexOf(superscriptSubStr) + superscriptSubStr.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
    )
    return spannableString
}