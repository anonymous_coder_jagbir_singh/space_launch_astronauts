package com.code.assignment.data.data

import com.code.assignment.data.data.client.AstronautsAPI
import io.reactivex.Observable
import junit.framework.TestCase.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


/**
 * Test cases for testing [AstronautsAPI] methods from
 * an integration (with server) point of view.
 *
 * PLEASE NOTE:
 * These test cases shall fail in case there is no
 * connectivity with the server.
 *
 */
class AstronautsAPIIntegrationTest {

    private lateinit var api: AstronautsAPI

    @Before
    @Throws(Exception::class)
    fun setUp() {

        val BASE_URL = "https://spacelaunchnow.me"

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()

        api = retrofit.create(AstronautsAPI::class.java)
    }

    @Test
    fun astronautsCallTest() {
        val testObserver = Observable.just(api.astronauts().execute()).test()

        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val values = testObserver.values()

        assertEquals(1, values.size)
        assertNotNull(values[0])
        assertNotNull(values[0].body())
        assertTrue(values[0].body()?.count ?: 0 > 0)
        if (values[0].body()?.results?.isNotEmpty() == true) {
            assertNotNull(values[0].body()?.results?.get(0)?.thumbnail?.isNotEmpty())
            assertNotNull(values[0].body()?.results?.get(0)?.name?.isNotEmpty())
            assertNotNull(values[0].body()?.results?.get(0)?.nationality?.isNotEmpty())
        }
    }

    @Test
    fun astronautDetailsCallTest() {
        val testObserver = Observable.just(api.astronautDetails(276).execute()).test()

        testObserver.assertComplete()
        testObserver.assertNoErrors()

        val values = testObserver.values()

        assertEquals(1, values.size)
        assertNotNull(values[0])
        assertNotNull(values[0].body())
        assertNotNull(values[0].body()?.thumbnail?.isNotEmpty())
        assertNotNull(values[0].body()?.name?.isNotEmpty())
        assertNotNull(values[0].body()?.nationality?.isNotEmpty())
        assertNotNull(values[0].body()?.bio?.isNotEmpty())
        assertNotNull(values[0].body()?.dateOfBirth?.isNotEmpty())
    }
}