package com.code.assignment.data.data.mapper

import com.code.assignment.data.data.carrier.APIResult
import com.code.assignment.data.data.model.APIAstronautDetails
import com.code.assignment.data.data.model.APIStatus
import com.code.assignment.domain.carrier.SResult
import junit.framework.TestCase
import org.junit.Test

class APIAstronautDetailsMapperTest {

    @Test
    fun toDomainTest() {
        val mapper = APIAstronautDetailsMapper()
        val data = APIResult.Success(
                APIAstronautDetails(
                        123,
                        "asdfghjk",
                        "qwerty",
                        "zxcvbn",
                        status = APIStatus(
                                2,
                                "name"
                        ),
                        bio = "qwertyuiop asdfghjkl zxcvbnm",
                        image = "/qwertyuio.jpg",
                        dateOfBirth = "1234-09-09"
                )
        )
        val domain = mapper.toDomain(data)

        TestCase.assertNotNull(domain)
        val success = domain as? SResult.Success
        TestCase.assertNotNull(success)
        TestCase.assertNotNull(success?.data)
        TestCase.assertEquals(success?.data?.bio, data.data.bio)
        TestCase.assertEquals(success?.data?.name, data.data.name)
        TestCase.assertEquals(success?.data?.id, data.data.id)
        TestCase.assertEquals(success?.data?.dateOfBirth, data.data.dateOfBirth)
        TestCase.assertEquals(success?.data?.nationality, data.data.nationality)
        TestCase.assertEquals(success?.data?.thumbnail, data.data.thumbnail)
    }

}