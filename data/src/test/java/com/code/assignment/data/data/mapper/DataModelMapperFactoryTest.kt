package com.code.assignment.data.data.mapper

import com.code.assignment.data.data.model.APIAstronaut
import com.code.assignment.data.data.model.APIAstronautDetails
import com.code.assignment.data.data.model.APIAstronautModel
import com.code.assignment.domain.usecase.model.Astronaut
import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.domain.usecase.model.AstronautModel
import junit.framework.TestCase.assertEquals
import org.junit.Test

class DataModelMapperFactoryTest {

    @Test
    fun astronautModelMapperTest() {
        assertEquals(
                "The mapper instance returned is not of type: "
                        + APIAstronautModelMapper::class.java,
                DataModelMapperFactory().create<APIAstronautModel, AstronautModel>(
                        APIAstronautModel::class.java
                ).javaClass,
                APIAstronautModelMapper::class.java
        )
    }

    @Test
    fun astronautDetailsMapperTest() {
        assertEquals(
                "The mapper instance returned is not of type: "
                        + APIAstronautDetailsMapper::class.java,
                DataModelMapperFactory().create<APIAstronautDetails, AstronautDetails>(
                        APIAstronautDetails::class.java
                ).javaClass,
                APIAstronautDetailsMapper::class.java
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun noMapperFoundTest() {
        DataModelMapperFactory().create<APIAstronaut, Astronaut>(APIAstronaut::class.java).javaClass
    }

}