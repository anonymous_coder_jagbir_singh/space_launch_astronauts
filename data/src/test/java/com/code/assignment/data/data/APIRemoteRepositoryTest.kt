package com.code.assignment.data.data

import com.google.gson.GsonBuilder
import com.code.assignment.data.data.callback.APICallbackFactory
import com.code.assignment.data.data.client.AstronautsAPI
import com.code.assignment.data.data.mapper.DataModelMapperFactory
import com.code.assignment.data.data.model.APIAstronaut
import com.code.assignment.data.data.model.APIAstronautDetails
import com.code.assignment.data.data.model.APIAstronautModel
import com.code.assignment.data.data.model.APIStatus
import okhttp3.Request
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.spy
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APIRemoteRepositoryTest : DataTestCase() {

    @Mock
    private lateinit var api: AstronautsAPI
    private lateinit var dataModelMapperFactory: DataModelMapperFactory
    private lateinit var apiCallbackFactory: APICallbackFactory
    private lateinit var apiRemoteRepository: APIRemoteRepository
    private val gson = GsonBuilder().create()

    private val defaultResponse: APIAstronautModel
        get() {
            return APIAstronautModel(
                    count = 600,
                    results = listOf(
                            default
                    )
            )
        }

    private val default: APIAstronaut
        get() {
            return APIAstronaut(
                    1,
                    "asdfghjk",
                    "qwerty",
                    "zxcvbn",
                    status = APIStatus(
                            2,
                            "name"
                    )
            )
        }

    override fun setUp() {
        super.setUp()
        dataModelMapperFactory = DataModelMapperFactory()
        apiCallbackFactory = spy(APICallbackFactory())
        apiRemoteRepository = APIRemoteRepository(
                api,
                dataModelMapperFactory,
                apiCallbackFactory,
                gson
        )
    }

    @Test
    fun astronautsAPITest() {
        val page = 1
        mockDefaultResponse(page, Response.success(defaultResponse))
        apiRemoteRepository.astronauts().test()
        Mockito.verify(api).astronauts()
    }

    @Test
    fun astronautDetailsAPITest() {
        val response = APIAstronautDetails(
                123,
                "asdfghjk",
                "qwerty",
                "zxcvbn",
                status = APIStatus(
                        2,
                        "name"
                ),
                bio = "qwertyuiop asdfghjkl zxcvbnm",
                dateOfBirth = "/qwertyuio.jpg",
                image = "/qwerty.jpg"
        )
        val mockCall = MockAPICall(Response.success(response))
        Mockito.`when`(api.astronautDetails(123)).thenReturn(mockCall)
        apiRemoteRepository.astronautDetails(123).test()
        Mockito.verify(api).astronautDetails(123)
    }

    private fun mockDefaultResponse(page: Int, response: Response<APIAstronautModel>) {
        val mockCall = MockAPICall(response)
        Mockito.`when`(api.astronauts()).thenReturn(mockCall)
    }

    private fun <T> any(): T = Mockito.any<T>()

    private class MockAPICall<DATA>(
            private val response: Response<DATA>
    ) : Call<DATA> {
        override fun enqueue(callback: Callback<DATA>) {
            execute()
        }

        override fun isExecuted(): Boolean {
            return true
        }

        override fun clone(): Call<DATA> {
            return this
        }

        override fun isCanceled(): Boolean {
            return false
        }

        override fun cancel() {
        }

        override fun execute(): Response<DATA> {
            return response
        }

        override fun request(): Request {
            return Request.Builder().build()
        }
    }

}