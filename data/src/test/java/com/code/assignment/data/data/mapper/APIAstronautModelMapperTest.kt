package com.code.assignment.data.data.mapper

import com.code.assignment.data.data.carrier.APIResult
import com.code.assignment.data.data.model.APIAstronaut
import com.code.assignment.data.data.model.APIAstronautModel
import com.code.assignment.data.data.model.APIStatus
import com.code.assignment.domain.carrier.SResult
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.Test

class APIAstronautModelMapperTest {

    @Test
    fun toDomainTest() {
        val mapper = APIAstronautModelMapper()
        val data = APIResult.Success(
                APIAstronautModel(
                        count = 600,
                        results = listOf(
                                APIAstronaut(
                                        1,
                                        "asdfghjk",
                                        "qwerty",
                                        "zxcvbn",
                                        status = APIStatus(
                                                2,
                                                "name"
                                        )
                                )
                        )
                )
        )
        val domain = mapper.toDomain(data)

        assertNotNull(domain)
        val success = domain as? SResult.Success
        assertNotNull(success)
        assertNotNull(success?.data)
        assertEquals(success?.data?.count, data.data.count)
        assertEquals(success?.data?.results?.size, data.data.results?.size)
        assertNotNull(success?.data?.results?.get(0))
        assertEquals(success?.data?.results?.get(0)?.name, data.data.results?.get(0)?.name)
        assertEquals(success?.data?.results?.get(0)?.id, data.data.results?.get(0)?.id)
        assertEquals(success?.data?.results?.get(0)?.thumbnail, data.data.results?.get(0)?.thumbnail)
        assertEquals(success?.data?.results?.get(0)?.nationality, data.data.results?.get(0)?.nationality)
        assertEquals(success?.data?.results?.get(0)?.status, data.data.results?.get(0)?.status?.name)
    }

}