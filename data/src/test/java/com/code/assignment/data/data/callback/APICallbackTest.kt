package com.code.assignment.data.data.callback

import com.google.gson.GsonBuilder
import com.code.assignment.data.data.DataTestCase
import com.code.assignment.data.data.model.APIAstronaut
import com.code.assignment.data.data.model.APIAstronautModel
import com.code.assignment.data.data.model.APIStatus
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.model.AstronautModel
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import okhttp3.Request
import org.junit.Test
import org.mockito.Mockito.spy
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APICallbackTest : DataTestCase() {

    private val dataModelMapperFactory = DataModelMapperFactory()

    private val defaultResponse: APIAstronautModel
        get() {
            return APIAstronautModel(
                    count = 600,
                    results = listOf(
                            default
                    )
            )
        }

    private val default: APIAstronaut
        get() {
            return APIAstronaut(
                    1,
                    "asdfghjk",
                    "qwerty",
                    "zxcvbn",
                    status = APIStatus(
                            2,
                            "name"
                    )
            )
        }

    @Test
    fun onFailureProcessingTest() {
        val errorMsg = "test error"
        val testListener = spy<(SResult<AstronautModel>) -> Unit> {
            val result = it as? SResult.Error
            assertNotNull(result)
            assertEquals(result?.error?.message, errorMsg)
        }
        val callback =
                spy(
                        APICallback(
                                APIAstronautModel::class.java,
                                dataModelMapperFactory,
                                testListener,
                                GsonBuilder().create()
                        )
                )
        callback.onFailure(
                MockAPICall(Response.success(defaultResponse)),
                IllegalArgumentException(errorMsg)
        )
    }

    @Test
    fun onSuccessProcessingTest() {
        val testListener = spy<(SResult<AstronautModel>) -> Unit> {
            val result = it as? SResult.Success
            assertNotNull(result)
            assertEquals(result?.data?.count, defaultResponse.count)
            assertEquals(result?.data?.results?.size, defaultResponse.results?.size)
            assertEquals(
                    result?.data?.results?.get(0)?.thumbnail,
                    defaultResponse.results?.get(0)?.thumbnail
            )
            assertEquals(
                    result?.data?.results?.get(0)?.name,
                    defaultResponse.results?.get(0)?.name
            )
        }
        val callback = spy(
                APICallback(
                        APIAstronautModel::class.java,
                        dataModelMapperFactory,
                        testListener,
                        GsonBuilder().create()
                )
        )
        callback.onResponse(
                MockAPICall(Response.success(defaultResponse)),
                Response.success(defaultResponse)
        )
    }

    private class MockAPICall(
            private val response: Response<APIAstronautModel>
    ) : Call<APIAstronautModel> {
        override fun enqueue(callback: Callback<APIAstronautModel>) {
            execute()
        }

        override fun isExecuted(): Boolean {
            return true
        }

        override fun clone(): Call<APIAstronautModel> {
            return this
        }

        override fun isCanceled(): Boolean {
            return false
        }

        override fun cancel() {
        }

        override fun execute(): Response<APIAstronautModel> {
            return response
        }

        override fun request(): Request {
            return Request.Builder().build()
        }
    }
}