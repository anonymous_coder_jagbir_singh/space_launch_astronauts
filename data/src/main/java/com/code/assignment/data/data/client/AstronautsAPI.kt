package com.code.assignment.data.data.client

import com.code.assignment.data.data.model.APIAstronautDetails
import com.code.assignment.data.data.model.APIAstronautModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * The Retrofit client for respective API calls.
 *
 */
interface AstronautsAPI {

    @GET("/api/3.5.0/astronaut/?format=json")
    fun astronauts(): Call<APIAstronautModel>

    @GET("/api/3.5.0/astronaut/{id}/?format=json")
    fun astronautDetails(@Path("id") id: Int): Call<APIAstronautDetails>

}