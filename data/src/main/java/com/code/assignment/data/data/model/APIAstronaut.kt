package com.code.assignment.data.data.model

import com.google.gson.annotations.SerializedName

open class APIAstronaut(
        @SerializedName("id")
        val id: Int?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("nationality")
        val nationality: String?,
        @SerializedName("profile_image_thumbnail")
        val thumbnail: String?,
        @SerializedName("status")
        val status: APIStatus?
)