package com.code.assignment.data.data.model

import com.google.gson.annotations.SerializedName

class APIErrorBody(
        @SerializedName("detail")
        val detail: String
)