package com.code.assignment.data.data.model

import com.google.gson.annotations.SerializedName

class APIStatus(
        @SerializedName("id")
        val id: Int?,
        @SerializedName("name")
        val name: String?
)