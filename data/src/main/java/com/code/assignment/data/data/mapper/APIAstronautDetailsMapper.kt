package com.code.assignment.data.data.mapper

import com.code.assignment.data.data.model.APIAstronautDetails
import com.code.assignment.domain.usecase.model.AstronautDetails

class APIAstronautDetailsMapper : APIBaseMapper<APIAstronautDetails, AstronautDetails>() {

    override fun modelToDomain(dataModel: APIAstronautDetails): AstronautDetails {
        dataModel.apply {
            return AstronautDetails(
                    id = id ?: 0,
                    name = name.orEmpty(),
                    nationality = nationality.orEmpty(),
                    thumbnail = thumbnail.orEmpty(),
                    bio = bio.orEmpty(),
                    dateOfBirth = dateOfBirth.orEmpty(),
                    image = image.orEmpty(),
                    status = status?.name ?: "--"
            )
        }
    }
}