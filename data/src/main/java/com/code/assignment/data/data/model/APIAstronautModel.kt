package com.code.assignment.data.data.model

import com.google.gson.annotations.SerializedName

class APIAstronautModel(
        @SerializedName("results")
        val results: List<APIAstronaut>? = null,
        @SerializedName("count")
        val count: Int? = null
)