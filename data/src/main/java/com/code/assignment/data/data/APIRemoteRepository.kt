package com.code.assignment.data.data

import com.google.gson.Gson
import com.code.assignment.data.data.callback.APICallbackFactory
import com.code.assignment.data.data.client.AstronautsAPI
import com.code.assignment.data.data.mapper.DataModelMapperFactory
import com.code.assignment.data.data.model.APIAstronautDetails
import com.code.assignment.data.data.model.APIAstronautModel
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.domain.usecase.model.AstronautModel
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Implementation of [RemoteRepository] and helps fetch data from the remote sever
 * via [AstronautsAPI].
 * The data is then mapped into DOMAIN layer models via mappers that are provided through [DataModelMapperFactory]
 */
class APIRemoteRepository @Inject constructor(
        private val api: AstronautsAPI,
        private val dataMapperFactory: DataModelMapperFactory,
        private val apiCallbackFactory: APICallbackFactory,
        private val gson: Gson
) : RemoteRepository {

    override fun astronauts(): Observable<SResult<AstronautModel>> {
        return Observable.create<SResult<AstronautModel>> { emitter ->
            api.astronauts().enqueue(
                    apiCallbackFactory.provideAPICallback<APIAstronautModel, AstronautModel>(
                            dataModelType = APIAstronautModel::class.java,
                            dataModelMapperFactory = dataMapperFactory,
                            gson = gson
                    ) {
                        emitter.onNext(it)
                        emitter.onComplete()
                    }
            )
        }
    }

    override fun astronautDetails(id: Int): Observable<SResult<AstronautDetails>> {
        return Observable.create<SResult<AstronautDetails>> { emitter ->
            api.astronautDetails(id).enqueue(
                    apiCallbackFactory.provideAPICallback<APIAstronautDetails, AstronautDetails>(
                            dataModelType = APIAstronautDetails::class.java,
                            dataModelMapperFactory = dataMapperFactory,
                            gson = gson
                    ) {
                        emitter.onNext(it)
                        emitter.onComplete()
                    }
            )
        }
    }
}