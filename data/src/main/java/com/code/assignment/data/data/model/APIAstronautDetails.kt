package com.code.assignment.data.data.model

import com.google.gson.annotations.SerializedName

class APIAstronautDetails(
        id: Int?,
        name: String?,
        nationality: String?,
        thumbnail: String?,
        status: APIStatus,
        @SerializedName("date_of_birth")
        val dateOfBirth: String?,
        @SerializedName("bio")
        val bio: String?,
        @SerializedName("profile_image")
        val image: String?
) : APIAstronaut(
        id,
        name,
        nationality,
        thumbnail,
        status
)