package com.code.assignment.data.data.mapper

import com.code.assignment.data.data.model.APIAstronautModel
import com.code.assignment.domain.usecase.model.Astronaut
import com.code.assignment.domain.usecase.model.AstronautModel

class APIAstronautModelMapper : APIBaseMapper<APIAstronautModel, AstronautModel>() {

    override fun modelToDomain(dataModel: APIAstronautModel): AstronautModel {
        dataModel.apply {
            return AstronautModel(
                    count = count ?: 0,
                    results = results?.map {
                        Astronaut(
                                id = it.id ?: 0,
                                name = it.name.orEmpty(),
                                nationality = it.nationality.orEmpty(),
                                thumbnail = it.thumbnail.orEmpty(),
                                status = it.status?.name ?: "--"
                        )
                    } ?: emptyList()
            )
        }
    }
}