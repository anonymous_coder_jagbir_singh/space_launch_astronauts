package com.code.assignment.data.data.callback

import com.google.gson.Gson
import com.code.assignment.data.data.mapper.DataModelMapperFactory
import com.code.assignment.domain.carrier.SResult
import javax.inject.Inject

class APICallbackFactory @Inject constructor() {

    fun <DATA, DOMAIN> provideAPICallback(
            dataModelType: Class<DATA>,
            dataModelMapperFactory: DataModelMapperFactory,
            gson: Gson,
            listener: (SResult<DOMAIN>) -> Unit
    ): APICallback<DATA, DOMAIN> {
        return APICallback(dataModelType, dataModelMapperFactory, listener, gson)
    }

}