package com.code.assignment.domain.repository

import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.usecase.model.AstronautDetails
import com.code.assignment.domain.usecase.model.AstronautModel
import io.reactivex.Observable

/**
 * The interface which defined the interactions which REMOTE world.
 *
 */
interface RemoteRepository {

    /**
     * fetch Astronauts content
     *
     * @return Observable<BResult<AstronautModel>>
     */
    fun astronauts(): Observable<SResult<AstronautModel>>

    /**
     * fetch Astronaut Details
     *
     * @return Observable<BResult<AstronautDetails>>
     */
    fun astronautDetails(id: Int): Observable<SResult<AstronautDetails>>

}