package com.code.assignment.domain.usecase.model

open class Astronaut(
        val id: Int,
        val name: String,
        val nationality: String,
        val thumbnail: String,
        val status: String
)