package com.code.assignment.domain.usecase.model

class AstronautModel(
        val results: List<Astronaut>,
        val count: Int
)