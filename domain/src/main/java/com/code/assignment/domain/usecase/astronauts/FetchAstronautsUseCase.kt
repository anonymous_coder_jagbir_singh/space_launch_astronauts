package com.code.assignment.domain.usecase.astronauts

import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.thread.ObserverThread
import com.code.assignment.domain.usecase.ObservableUseCase
import com.code.assignment.domain.usecase.model.AstronautModel
import io.reactivex.Observable
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * The use-case to fetch the list of astronauts from the remote API.
 */
class FetchAstronautsUseCase @Inject internal constructor(
        observerThread: ObserverThread,
        threadExecutor: Executor,
        private val remoteRepository: RemoteRepository
) : ObservableUseCase<SResult<AstronautModel>, Unit>(observerThread, threadExecutor) {

    override fun constructObservable(params: Unit?): Observable<SResult<AstronautModel>> {
        return remoteRepository.astronauts()
    }
}