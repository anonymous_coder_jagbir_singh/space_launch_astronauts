package com.code.assignment.domain.usecase.austronautdetail

import com.code.assignment.domain.carrier.SError
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.thread.ObserverThread
import com.code.assignment.domain.usecase.ObservableUseCase
import com.code.assignment.domain.usecase.model.AstronautDetails
import io.reactivex.Observable
import java.util.concurrent.Executor
import javax.inject.Inject

/**
 * The use-case to fetch the details of a particular astronaut from the remote API.
 */
class FetchAstronautDetailsUseCase @Inject internal constructor(
        observerThread: ObserverThread,
        threadExecutor: Executor,
        private val remoteRepository: RemoteRepository
) : ObservableUseCase<SResult<AstronautDetails>, Int>(observerThread, threadExecutor) {

    override fun constructObservable(params: Int?): Observable<SResult<AstronautDetails>> {
        return params?.let { id ->
            remoteRepository.astronautDetails(id)
        } ?: Observable.just(SResult.Error(SError("Invalid params")))
    }
}