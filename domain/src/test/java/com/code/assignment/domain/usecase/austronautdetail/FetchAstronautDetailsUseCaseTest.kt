package com.code.assignment.domain.usecase.austronautdetail

import com.code.assignment.domain.DomainTestCase
import com.code.assignment.domain.carrier.SError
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.thread.ObserverThread
import com.code.assignment.domain.usecase.model.AstronautDetails
import io.reactivex.Observable
import junit.framework.TestCase
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.util.concurrent.Executor

class FetchAstronautDetailsUseCaseTest : DomainTestCase() {

    @Mock
    lateinit var repository: RemoteRepository

    @Mock
    lateinit var observerThread: ObserverThread

    @Mock
    lateinit var executor: Executor

    private lateinit var useCase: FetchAstronautDetailsUseCase

    private val defaultResponse: SResult<AstronautDetails>
        get() {
            val model = AstronautDetails(
                    1,
                    "asdfghjk",
                    "qwerty",
                    "zxcvbn",
                    "02-02-02",
                    "qwertyuiop asdfghjkl zxcvbnm",
                    "/qwertyuio.jpg",
                    "status"
            )
            return SResult.Success(model)
        }

    private val errorDataResponse: SResult<AstronautDetails>
        get() {
            return SResult.Error(error = SError("something went wrong"))
        }

    override fun setUp() {
        super.setUp()
        useCase =
                FetchAstronautDetailsUseCase(
                        observerThread,
                        executor,
                        repository
                )
    }

    @Test
    fun `WHEN use-case is subscribed THEN it gets completed successfully`() {
        val id = 123
        Mockito.`when`(repository.astronautDetails(id)).thenReturn(Observable.just(defaultResponse))
        val observer = useCase.constructObservable(id).test()
        observer.assertComplete()
    }

    @Test
    fun `WHEN use-case is subscribed THEN correct repository method is called`() {
        val id = 123
        Mockito.`when`(repository.astronautDetails(id)).thenReturn(Observable.just(defaultResponse))
        useCase.constructObservable(id).test()
        Mockito.verify(repository, Mockito.times(1)).astronautDetails(id)
    }

    @Test
    fun `GIVEN use-case is subscribed WHEN repository is called THEN correct data is returned`() {
        val response = defaultResponse
        val id = 123
        Mockito.`when`(repository.astronautDetails(id)).thenReturn(Observable.just(response))
        val observer = useCase.constructObservable(id).test()
        observer.assertValue(response)
    }

    @Test
    fun `GIVEN use-case is subscribed WHEN repository returns error THEN error is returned`() {
        val id = 123
        Mockito.`when`(repository.astronautDetails(id)).thenReturn(Observable.just(errorDataResponse))
        val observer = useCase.constructObservable(id).test()
        val result = observer.values()[0]
        TestCase.assertTrue(result is SResult.Error)
        TestCase.assertTrue(result != null)
    }

}