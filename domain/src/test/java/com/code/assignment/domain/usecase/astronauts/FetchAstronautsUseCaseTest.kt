package com.code.assignment.domain.usecase.astronauts

import com.code.assignment.domain.DomainTestCase
import com.code.assignment.domain.carrier.SError
import com.code.assignment.domain.carrier.SResult
import com.code.assignment.domain.repository.RemoteRepository
import com.code.assignment.domain.thread.ObserverThread
import com.code.assignment.domain.usecase.model.Astronaut
import com.code.assignment.domain.usecase.model.AstronautModel
import io.reactivex.Observable
import junit.framework.TestCase
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.util.concurrent.Executor

class FetchAstronautsUseCaseTest : DomainTestCase() {

    @Mock
    lateinit var repository: RemoteRepository

    @Mock
    lateinit var observerThread: ObserverThread

    @Mock
    lateinit var executor: Executor

    private lateinit var useCase: FetchAstronautsUseCase

    private val defaultResponse: SResult<AstronautModel>
        get() {
            val model = AstronautModel(
                    count = 600,
                    results = listOf(
                            default
                    )
            )
            return SResult.Success(model)
        }

    private val errorDataResponse: SResult<AstronautModel>
        get() {
            return SResult.Error(error = SError("something went wrong"))
        }

    private val default: Astronaut
        get() {
            return Astronaut(
                    1,
                    "asdfghjk",
                    "qwerty",
                    "zxcvbn",
                    "status"
            )
        }

    override fun setUp() {
        super.setUp()
        useCase =
                FetchAstronautsUseCase(
                        observerThread,
                        executor,
                        repository
                )
    }

    @Test
    fun `WHEN use-case is subscribed THEN it gets completed successfully`() {
        Mockito.`when`(repository.astronauts()).thenReturn(Observable.just(defaultResponse))
        val observer = useCase.constructObservable(Unit).test()
        observer.assertComplete()
    }

    @Test
    fun `WHEN use-case is subscribed THEN correct repository method is called`() {
        Mockito.`when`(repository.astronauts()).thenReturn(Observable.just(defaultResponse))
        useCase.constructObservable(Unit).test()
        Mockito.verify(repository, Mockito.times(1)).astronauts()
    }

    @Test
    fun `GIVEN use-case is subscribed WHEN repository is called THEN correct data is returned`() {
        val response = defaultResponse
        Mockito.`when`(repository.astronauts()).thenReturn(Observable.just(response))
        val observer = useCase.constructObservable(Unit).test()
        observer.assertValue(response)
    }

    @Test
    fun `GIVEN use-case is subscribed WHEN repository returns error THEN error is returned`() {
        Mockito.`when`(repository.astronauts()).thenReturn(Observable.just(errorDataResponse))
        val observer = useCase.constructObservable(Unit).test()
        val result = observer.values()[0]
        TestCase.assertTrue(result is SResult.Error)
        TestCase.assertTrue(result != null)
    }

}